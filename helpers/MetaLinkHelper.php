<?php

namespace spaitnet\helpers;

use yii\helpers\Url;
use yii\web\View;

class MetaLinkHelper
{
    /**
     * @param View $view
     * @param  string $desc
     * @param  boolean $checkFirst
     */
    public static function registerDescription($view, $desc, $checkFirst = true)
    {
        if (!empty($desc) && (!$checkFirst || empty($view->metaTags['MetaLinkHelper.description']))) {
            $view->registerMetaTag([
                'name'    => 'Description',
                'content' => $desc,
            ], 'MetaLinkHelper.description');
        }
    }

    /**
     * @param View $view
     * @param  string $keywords
     * @param  boolean $checkFirst
     */
    public static function registerKeywords($view, $keywords, $checkFirst = true)
    {
        if (!empty($keywords) && (!$checkFirst || empty($view->metaTags['MetaLinkHelper.keywords']))) {
            $view->registerMetaTag([
                'name'    => 'Keywords',
                'content' => $keywords,
            ], 'MetaLinkHelper.keywords');
        }
    }

    /**
     * @param View $view
     * @param  string|array $url
     */
    public static function registerCanonical($view, $url)
    {
        $href = Url::to($url, true);
        if (!empty($href)) {
            $view->registerLinkTag([
                'rel'  => 'canonical',
                'href' => $href,
            ], 'MetaLinkHelper.canonical');
        }
    }

    /**
     * @param View $view
     */
    public static function registerNoindexNofollow($view)
    {
        $view->registerMetaTag([
            'name' => 'robots',
            'content' => 'noindex, nofollow',
        ]);
    }

    /**
     * @param View $view
     * @param array $data
     */
    public static function registerOg($view, $data)
    {
        foreach ($data as $name => $content) {
            $view->registerMetaTag([
                'property' => 'og:'.$name,
                'content' => $content,
            ], 'MetaLinkHelper.og.'.$name);
        }
    }
}